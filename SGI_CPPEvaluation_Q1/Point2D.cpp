#include "stdafx.h"
#include "Point2D.h"

Point2D::Point2D()
{
	// If no parameters are passed, we will initialize at (0,0)
	x = 0;
	y = 0;
}

Point2D::Point2D(float width, float height)
{
	x = width;
	y = height;
}


Point2D::~Point2D()
{
}

float Point2D::getX()
{
	return x;
}

float Point2D::getY()
{
	return y;
}
