#pragma once

#include "Point2D.h"

class Rectangle
{
private:
	Point2D pBottomLeft;
	Point2D pTopRight;

public:
	Rectangle(float x, float y, float width, float height);
	Rectangle(const Rectangle &rect);
	~Rectangle();

	Rectangle &operator=(const Rectangle &rect);

	bool Contains(Point2D);
	bool Intersects(Rectangle);
	void PrintInfo();
};

