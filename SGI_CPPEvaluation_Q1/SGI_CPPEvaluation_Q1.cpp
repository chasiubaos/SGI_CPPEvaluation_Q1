// SGI_CPPEvaluation_Q1.cpp : Defines the entry point for the console application.

#include <cstdio>
#include "stdafx.h"
#include "Rectangle.h"
#include "Point2D.h"

using namespace std;

int main()
{
	// Creating rectangles
	Rectangle rect1 = Rectangle(0, 0, 1.0, 1.0);
	Rectangle rect2 = rect1;
	Rectangle rect3 = Rectangle(rect1);

	Rectangle overlapRectBase = rect1;

	Rectangle overlapRect1 = Rectangle(0.5, 0.5, 1.0, 1.0);		// Overlaps bottom left corner
	Rectangle overlapRect2 = Rectangle(-0.5, -0.5, 1.0, 1.0);	// Overlaps top right corner
	Rectangle overlapRect3 = Rectangle(0.5, -0.5, 1.0, 1.0);	// Overlaps top left corner
	Rectangle overlapRect4 = Rectangle(-0.5, 0.5, 1.0, 1.0);	// Overlaps bottom right corner
	Rectangle overlapRect5 = Rectangle(-0.5, -0.5, 2.0, 2.0);	// Completely overlaps rectangle
	Rectangle overlapRect6 = Rectangle(-0.5, -0.5, 1.0, 2.0);	// Overlaps left side
	Rectangle overlapRect7 = Rectangle(0.5, -0.5, 1.0, 2.0);	// Overlaps right side
	Rectangle overlapRect8 = Rectangle(-0.5, 0.5, 2.0, 1.0);	// Overlaps top side
	Rectangle overlapRect9 = Rectangle(-0.5, -0.5, 2.0, 1.0);	// Overlaps bottom side
	Rectangle overlapRect10 = Rectangle(0.25, 0.25, 0.5, 0.5);	// Lies within base rectangle

	Rectangle nOverlapRect1 = Rectangle(0, 2, 1, 1);			// Rectangle above
	Rectangle nOverlapRect2 = Rectangle(0, -2, 1, 1);			// Rectangle below
	Rectangle nOverlapRect3 = Rectangle(-1, 0, 1, 1);			// Rectangle to left
	Rectangle nOverlapRect4 = Rectangle(2, 0, 1, 1);			// Rectangle to right

	Point2D containedPoint = Point2D(0.5, 0.5);
	Point2D notContainedPoint = Point2D(-0.5, -0.5);

	// Check the copy constructor and assignment operator override. They should all be the same rectangle.
	rect1.PrintInfo();
	rect2.PrintInfo();
	rect3.PrintInfo();

	// Check all the overlap and not overlap cases. Should be 10 overlaps and 4 not overlaps.
	overlapRectBase.Intersects(overlapRect1);
	overlapRectBase.Intersects(overlapRect2);
	overlapRectBase.Intersects(overlapRect3);
	overlapRectBase.Intersects(overlapRect4);
	overlapRectBase.Intersects(overlapRect5);
	overlapRectBase.Intersects(overlapRect6);
	overlapRectBase.Intersects(overlapRect7);
	overlapRectBase.Intersects(overlapRect8);
	overlapRectBase.Intersects(overlapRect9);
	overlapRectBase.Intersects(overlapRect10);
	overlapRectBase.Intersects(nOverlapRect1);
	overlapRectBase.Intersects(nOverlapRect2);
	overlapRectBase.Intersects(nOverlapRect3);
	overlapRectBase.Intersects(nOverlapRect4);

	// Check a point that is in the rectangle and one that is not
	overlapRectBase.Contains(containedPoint);
	overlapRectBase.Contains(notContainedPoint);

	getchar();
    return 0;
}

