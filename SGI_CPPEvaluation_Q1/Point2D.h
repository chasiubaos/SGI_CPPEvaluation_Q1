#pragma once
class Point2D
{
private:
	float x;
	float y;

public:
	Point2D();
	Point2D(float, float);
	~Point2D();

	float getX();
	float getY();
};

