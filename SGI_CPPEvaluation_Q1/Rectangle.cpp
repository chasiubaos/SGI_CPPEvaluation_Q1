#include <stdio.h>
#include "stdafx.h"
#include "Rectangle.h"
#include "Point2D.h"

using namespace std;

Rectangle::Rectangle(float xBottomLeft, float yBottomLeft, float width, float height)
{
	pBottomLeft = Point2D(xBottomLeft, yBottomLeft);
	pTopRight = Point2D(xBottomLeft + width, yBottomLeft + height);
}

Rectangle::Rectangle(const Rectangle & rect)
{
	pBottomLeft = rect.pBottomLeft;
	pTopRight = rect.pTopRight;
}

Rectangle::~Rectangle()
{
}

Rectangle & Rectangle::operator=(const Rectangle &rect)
{
	if (this == &rect)
		return *this;

	this->pBottomLeft = rect.pBottomLeft;
	this->pTopRight = rect.pTopRight;

	return *this;
}

bool Rectangle::Contains(Point2D point)
{
	if (point.getX() >= this->pBottomLeft.getX() &&
		point.getX() <= this->pTopRight.getX() &&
		point.getY() >= this->pBottomLeft.getY() &&
		point.getY() <= this->pTopRight.getY())
	{
		printf("Rectangle contains point!!\n");
		return true;
	}

	printf("Rectangle does not contain point!!\n");
	return false;
}

bool Rectangle::Intersects(Rectangle rectangle)
{
	if (this->pBottomLeft.getX() < rectangle.pTopRight.getX() &&
		this->pBottomLeft.getY() < rectangle.pTopRight.getY() &&
		this->pTopRight.getX() > rectangle.pBottomLeft.getX() &&
		this->pTopRight.getY() > rectangle.pBottomLeft.getY())
	{
		printf("Rectangles intersect!!\n");
		return true;
	}
	
	printf("Rectangles do not intersect!!\n");
	return false;
}

void Rectangle::PrintInfo()
{
	printf("Rectangle Info:\n");
	printf("Bottom Left Coordinate: (%3.2f, %3.2f)\n", this->pBottomLeft.getX(), this->pBottomLeft.getY());
	printf("Top Right Coordinate: (%3.2f, %3.2f)\n", this->pTopRight.getX(), this->pTopRight.getY());
}
